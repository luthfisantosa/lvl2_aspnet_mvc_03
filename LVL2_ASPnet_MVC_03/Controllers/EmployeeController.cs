﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace LVL2_ASPnet_MVC_03.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();

        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var removeRecord = db.Employees.Where(x => x.Id == 1).FirstOrDefault();
                    db.Employees.Remove(removeRecord);
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("index");
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    return RedirectToAction("index");
                }
            }
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(int id, Employee employee)
        {
            try
            {
                db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                ViewBag.Result = ex.Message;
            }

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Delete(int id, Employee employee)
        {
            try
            {
                db.Entry(employee).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                ViewBag.Result = ex.Message;
            }

            return RedirectToAction("Index");
        }
    }
}